ODIR = obj
FLAGS = -Wno-write-strings -fpermissive -Wno-format-security -Wno-format -I/usr/local/include
OBJ = $(ODIR)/DataProcess.o $(ODIR)/fl_ext_file_chooser.o $(ODIR)/Fl_PlotXY.o $(ODIR)/main.o $(ODIR)/Rotated.o $(ODIR)/k-means.o
LIBRARIES = -lfltk -lXext -lXpm -lfann -lm -lstdc++ -Wl,-Bsymbolic-functions -lfltk -lX11 -lfltk_images

$(ODIR)/%.o: %.cpp
	g++ -c -o $@ $(FLAGS) $< -I.
$(ODIR)/%.o: %.cxx 
	g++ -c -o $@ $(FLAGS) $< -I.
$(ODIR)/%.o: %.C 
	g++ -c -o $@ $(FLAGS) $< -I.
$(ODIR)/%.o: %.c
	gcc -c -o $@ $(FLAGS) $< -I.
FannTool: $(OBJ)
	gcc -o $@ $^ -I. $(LIBRARIES)
clean:
	rm $(ODIR)/*.o
